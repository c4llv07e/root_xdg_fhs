ip address flush dev eth
ip route flush dev eth
ip link set dev eth up
ip address add 192.168.0.2/24 brd + dev eth
ip address add 2a02:2168:84fe:1900::2/60 dev eth
ip route add default via 192.168.0.1 dev eth
ip -6 route add default via 2a02:2168:84fe:1900::1 dev eth
ip link set dev eth up
