local pack_path = vim.fs.joinpath(vim.fn.stdpath("data"),
  "site", "pack", "c4", "start")
local plugins = {
  ["telescope.nvim"] = "https://github.com/nvim-telescope/telescope.nvim",
  ["plenary.nvim"] = "https://github.com/nvim-lua/plenary.nvim",
  ["telescope-fzy-native.nvim"] = 
    "https://github.com/nvim-telescope/telescope-fzy-native.nvim.git",
  ["nvim-treesitter"] =
    "https://github.com/nvim-treesitter/nvim-treesitter.git",
  ["nvim-lspconfig"] =
    "https://github.com/neovim/nvim-lspconfig.git",
}
for k, v in pairs(plugins) do
  local out_path = vim.fs.joinpath(pack_path, k)
  if vim.fn.isdirectory(out_path) == 0 then
    local out = vim.fn.system(
      {"git", "clone", "--recurse-submodules", v, out_path})
    if vim.v.shell_error ~= 0 then
      vim.api.nvim_echo(
        {{"Error cloning " .. k .. " plugin:\n" .. out, "ErrorMsg"}},
        true, {})
    end
  end
end

vim.cmd("packloadall")
