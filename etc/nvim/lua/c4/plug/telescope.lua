require('telescope').setup({
  defaults = require('telescope.themes').get_ivy({
  }),
})
require('telescope').load_extension('fzy_native')

local builtin = require('telescope.builtin')
local function grep_regex()
  builtin.live_grep({
    use_regex = true,
  })
end
vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Telescope find files' })
vim.keymap.set('n', '<leader>fg', grep_regex, { desc = 'Telescope live grep' })
vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Telescope buffers' })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Telescope help tags' })
