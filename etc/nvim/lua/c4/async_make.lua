function async_make()
  local makeprg = vim.opt.makeprg:get()
  local errorformat = vim.opt.errorformat:get()[1]
  if not makeprg then return end

  local cmd = vim.fn.expandcmd(makeprg)

  local function on_line(job_id, new_lines, event)
    if new_lines then
      for _, line in ipairs(new_lines) do
        if line ~= "" then
          print(string.format("\"%s\"", line))
          vim.inspect(vim.fn.setqflist({}, 'a', { lines = {line} }))
        end
      end
    end
  end

  vim.fn.setqflist({}, ' ', {
    title = cmd,
    efm = errorformat,
  })

  local job_id =
    vim.fn.jobstart(
    cmd,
    {
      on_stderr = on_line,
      on_stdout = on_line,
      on_exit = on_exit,
      stdout_buffered = false,
      stderr_buffered = false,
    }
  )
end

vim.keymap.set('n', '<leader>am', async_make)
