#!/bin/sh
set -ue
CC=${CC:-tcc}
exec "${CC}" -static -O3 -std=c89 -Wall -Wextra -pedantic -o ./sinit \
	./sinit.c \
	/usr/lib/musl/lib/libc.a
