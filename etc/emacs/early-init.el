(setopt package-enable-at-startup nil)
(setq gc-cons-threshold (* 80 1024 1024))
(setq max-specpdl-size 5000)

(require 'xdg)
(startup-redirect-eln-cache
 (expand-file-name  "emacs/eln-cache/" (xdg-cache-home)))

(add-to-list 'load-path (locate-user-emacs-file "c4ll"))
(require 'c4ll_theme)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tooltip-mode) (tooltip-mode -1))
(if (fboundp 'set-fringe-mode) (set-fringe-mode -1))
(setq visible-bell nil)

(provide 'early-init)
