
;(setq modus-themes-custom-auto-reload nil
;      modus-themes-disable-other-themes t)

(setq modus-vivendi-palette-overrides
      '(
        (bg-main "#081508")
        (bg-dim  "#041004")
        (bg-alt  "#105010")

        (bg-line-number-active "#004000")

        (bg-mode-line-inactive "#102810")

        (border bg-dim)
        (bg-mode-line-active bg-alt)

        (bg-region "#211950")
        ))

(setq modus-themes-common-palette-overrides
      '(
        (border-mode-line-active unspecified)
        (border-mode-line-inactive unspecified)
        (fg-region unspecified)
        ))

(custom-set-faces
 '(mc/cursor-bar-face ((t (:background "#eeeeee" :height 3)))))
(load-theme 'modus-vivendi t)

(provide 'c4ll_theme)
