#!/usr/bin/sh
set -ue
udevd --daemon
udevadm trigger --action=add --type=subsystems
udevadm trigger --action=add --type=devices
udevadm settle
pkill udevd
