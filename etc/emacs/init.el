;; -*- coding: utf-8; lexical-binding: t -*-
(setq lexical-binding t)

(defvar native-comp-deferred-compilation-deny-list nil)

(require 'whitespace)

(setq custom-file (locate-user-emacs-file "custom-set-vars.el"))
(load custom-file 'noerror 'nomessage)

(defun display-startup-echo-area-message ()
  "Do nothing.")

(defun disable-line-numbers ()
  "Disable line numbers in current buffer."
  (display-line-numbers-mode 0))

(defun back-other-window ()
  "Go one window back."
  (interactive)
  (other-window -1))

(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

(setq custom-tab-width 8)
(setq-default tab-width custom-tab-width
              indent-tabs-mode t
              python-indent-offset 4
              c-ts-mode-indent-offset tab-width
              c-ts-mode-indent-style 'k&r
              go-ts-mode-indent-offset tab-width
              )

(setq-default native-comp-async-report-warnings-errors 'silent
              truncate-string-ellipsis "<"
              require-final-newline t
              sentence-end-double-space nil
              display-line-numbers 'relative
              cursor-type '(bar . 2)
              cursor-in-non-selected-windows '(hbar . 2)
              display-line-numbers-width 3
              fill-column 75
              whitespace-line-column fill-column
              )

(setq create-lockfiles nil
      make-backup-files nil
      auto-save-list-file-name nil
      auto-save-default nil
      display-time-day-and-date t
      display-time-24hr-format t
      display-time-format "%Y-%m-%dT%T"
      display-time-interval 1
      display-line-numbers 'relative
      xwidget-webkit-cookie-file (locate-user-emacs-file "webkit-cookie")
      history-length 10000
      package-install-upgrade-built-in t
      dired-auto-revert-buffer t
      inhibit-startup-message t
      inhibit-startup-echo-area-message t
      initial-scratch-message nil
      shift-select-mode nil
      copy-region-blink-delay 0 ; emacs, just why?
      use-file-dialog nil
      compile-command "./build.sh"
      enable-recursive-minibuffers t
      compilation-scroll-output t
      use-dialog-box nil
      inhibit-default-init t
      ns-pop-up-frames nil
      line-move-visual nil
      pop-up-windows t
      dabbrev-case-fold-search t
      ido-record-commands nil
      ido-enable-last-directory-history nil
      read-process-output-max (* 1024 1024)
      c-default-style "k&r"
      c-indentation-style "k&r"
      c-basic-offset custom-tab-width
      sh-basic-offset custom-tab-width
      smie-indent-basic custom-tab-width
      next-screen-context-lines 8
      next-error-message-highlight t
      display-time-default-load-average nil
      list-matching-lines-default-context-lines 0
      org-blank-before-new-entry '((heading . t) (plain-list-item . t))
      grep-find-command '("rg --vimgrep \"\"" . 15)
      whitespace-style '(face tabs spaces trailing space-before-tab
                              indentation space-after-tab space-mark
                              tab-mark)
      shell-command-prompt-show-cwd t
      backward-delete-char-untabify-method nil
      )

(setopt use-package-ensure-function 'ignore)
(setopt package-archives nil)
(setopt set-mark-command-repeat-pop t)
(setopt frame-inhibit-implied-resize t)

(set-face-foreground 'whitespace-hspace "#00a080")
(set-face-foreground 'whitespace-indentation "#00a080")
(set-face-foreground 'whitespace-newline "#00a080")
(set-face-foreground 'whitespace-space "#00a080")
(set-face-foreground 'whitespace-tab "#00a080")

(fset 'yes-or-no-p 'y-or-n-p)
(defalias 'yes-or-not-p 'y-or-n-p)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'minibuffer-history 'history-length 500)
(put 'evil-ex-history 'history-length 500)
(put 'kill-ring 'history-length 250)

(display-time-mode)
(column-number-mode)
(display-line-numbers-mode)
(global-subword-mode)
(transient-mark-mode 1)
(electric-indent-mode 0)
(minibuffer-depth-indicate-mode)
(global-whitespace-mode)
(global-display-fill-column-indicator-mode)

(blink-cursor-mode 0)
(set-frame-font "Liberation Mono 10" nil t)
(set-cursor-color "#00ffff")
(add-to-list 'default-frame-alist '(alpha-background . 90))
(require 'c4ll_theme)

(defvar c4-git-grep-history nil "History list for `c4-git-grep'.")
(defun c4-git-grep (command)
  (interactive
   (list (read-shell-command
          ""
          '("git --no-pager grep -I -n --color=auto -e \"\"" . 44)
          'c4-git-grep-history)))
  (select-window (display-buffer (get-buffer-create "*grep*")))
  (compilation-start command 'grep-mode))

(defvar c4-git-show-history nil "History list for `c4-git-show'.")
(defun c4-git-show (command)
  (interactive
   (list (read-shell-command
          ""
          '("git --no-pager show \"\"" . 22)
          'c4-git-show-history)))
  (compilation-start command nil (lambda (mode) "" "*git-show*")))

(add-hook 'eshell-mode-hook 'disable-line-numbers)
(add-hook 'shell-mode-hook 'disable-line-numbers)

(setq parens-require-spaces nil)
(add-hook 'emacs-lisp-mode-hook #'(lambda ()
                                    (setq-local parens-require-spaces t)))
(add-hook 'lisp-mode-hook #'(lambda ()
                              (setq-local parens-require-spaces t)))

(global-set-key (kbd "C-c m") 'man)
(global-set-key (kbd "M-n") 'find-file)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c n c") 'org-capture)
(global-set-key (kbd "C-c C-l") 'sort-lines)
(global-set-key (kbd "M-f") 'forward-word)
(global-set-key (kbd "M-F") 'forward-to-word)
(global-set-key (kbd "M-b") 'backward-word)
(global-set-key (kbd "M-B") 'backward-to-word)
(global-set-key (kbd "C-x g")
                #'(lambda ()
                    (interactive)
                    (select-window (display-buffer
                                    (get-buffer-create "*grep*")))
                    (call-interactively 'grep-find)))
(global-set-key (kbd "C-c C-a") 'c4-git-grep)
(global-set-key (kbd "C-c C-s") 'c4-git-show)
(global-set-key (kbd "<f5>") 'recompile)
(global-set-key (kbd "C-<f5>") 'compile)
(defun c4-insert-timestamp ()
  (interactive)
  (insert "[" (format-time-string "%Y-%m-%dT%H:%M:%S" (current-time)) "]"))
(global-set-key (kbd "C-c C-d") 'c4-insert-timestamp)

(global-set-key (kbd "M-B") 'backward-to-word)
(global-set-key (kbd "M-O") 'back-other-window)
(global-set-key (kbd "C-x O") 'back-other-window)
(global-set-key (kbd "M-o") 'other-window)
(add-hook 'html-mode-hook
          #'(lambda ()
              (define-key html-mode-map (kbd "M-o") nil)))
(add-hook 'conf-mode-hook
          #'(lambda ()
              (define-key conf-mode-map (kbd "C-c C-a") nil)))
(global-set-key (kbd "M-Z") 'zap-up-to-char)
(add-hook 'org-mode-hook
          #'(lambda ()
              (define-key org-mode-map (kbd "C-c t") 'org-timestamp)))
(add-hook 'markdown-mode-hook
          #'(lambda ()
              (define-key markdown-mode-map (kbd "C-c C-d") nil)
              (define-key markdown-mode-map (kbd "DEL")
                          'backward-delete-char-untabify)))
(add-hook 'csharp-mode-hook
          #'(lambda ()
              (define-key csharp-mode-map (kbd "C-c C-l") nil)
              (define-key csharp-mode-map (kbd "C-c C-a") nil)
              (define-key csharp-mode-map (kbd "C-c C-s") nil)))
(add-hook 'c-mode-hook
          #'(lambda ()
              (define-key c-mode-map (kbd "C-c C-l") nil)))
(add-hook 'python-mode-hook
          #'(lambda ()
              (define-key python-mode-map (kbd "C-c C-l") nil)))
(add-hook 'asm-mode-hook
          #'(lambda ()
              (define-key asm-mode-map (kbd ":") nil)))
(add-hook 'nxml-mode-hook
          #'(lambda ()
              (define-key nxml-mode-map (kbd "C-c C-s") nil)))

(defun insert-tab ()
  (interactive)
  (insert "\t"))


(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "C-z") 'zone)

(global-set-key (kbd "M-<left>")
                (lambda () (interactive) (shrink-window-horizontally 4)))
(global-set-key (kbd "M-<right>")
                (lambda () (interactive) (enlarge-window-horizontally 4)))
(global-set-key (kbd "M-<down>")
                (lambda () (interactive) (shrink-window 4)))
(global-set-key (kbd "M-<up>")
                (lambda () (interactive) (enlarge-window 4)))

(global-unset-key (kbd "C-c C-z"))
(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-x C-n"))

(defun my-indent-setup ()
  (c-set-offset 'arglist-intro '+))
(add-hook 'c-mode-hook 'my-indent-setup)

(defun c4ll/haskell-config ()
  (eldoc-mode -1))
(if (boundp 'haskell-mode-hook)
    (add-hook 'haskell-mode-hook 'c4ll/haskell-config))

(defun c4ll/init-package-manager ()
  (eval-and-compile
    (defvar bootstrap-version)
    ;; TEMPORARY issue with straight, see
    ;; https://jeffkreeftmeijer.com/emacs-straight-use-package/
    (setq straight-repository-branch "develop")
    (let ((bootstrap-file
           (expand-file-name "straight/repos/straight.el/bootstrap.el"
                             user-emacs-directory))
          (bootstrap-version 6))
      (unless (file-exists-p bootstrap-file)
        (with-current-buffer
            (url-retrieve-synchronously
             "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
             'silent 'inhibit-cookies)
          (goto-char (point-max))
          (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage))

    (straight-use-package '(use-package :type built-in))
    (require 'bind-key)
    (setq straight-use-package-by-default t)))

(defun c4ll/install-packages ()
  (c4ll/init-package-manager)

  (use-package rainbow-delimiters
    :hook (prog-mode . rainbow-delimiters-mode))

  (use-package haskell-mode
    :hook (haskell-mode . interactive-haskell-mode))

  (use-package proof-general
    :config (setq proof-three-window-mode-policy 'hybrid))

  (use-package editorconfig
    :config
    (setq-default editorconfig-trim-whitespaces-mode '(lambda (&rest body)))
    (editorconfig-mode 1)
    (add-hook 'editorconfig-after-apply-functions
              '(lambda (props)
                 (let ((width (gethash 'max_line_length props)))
                   (when (and (editorconfig-string-integer-p width)
                              (> (string-to-number width) 0))
                     (setq-local whitespace-line-column
                                 (string-to-number width)))))))

  (defalias 'yaml-mode 'yaml-ts-mode)

  (use-package markdown-mode
    :bind (("C-<return>" . markdown-follow-link-at-point)))

  (straight-use-package 'fasm-mode)

  (use-package gcmh
    :ensure t
    :diminish
    :init (setq gc-cons-threshold most-positive-fixnum)
    :hook (emacs-startup . gcmh-mode)
    :custom
    (gcmh-idle-delay 'auto)
    (gcmh-auto-idle-delay-factor 10)
    (gcmh-high-cons-threshold (* 16 1024 1024)))

  (use-package dtrt-indent
    :config
    (dtrt-indent-global-mode)
    (setq dtrt-indent-lighter nil
          dtrt-indent-run-after-smie t
          dtrt-indent-min-offset 2))

  (straight-use-package
   '(hare-mode :type git :host github :repo "alfredfo/hare-mode"))

  (straight-use-package 'aggressive-indent-mode)
  (straight-use-package 'latex-preview-pane)
  (straight-use-package 'restclient)
  (straight-use-package 'visual-fill-column)

  (use-package glsl-mode))
(c4ll/install-packages)

(setq auto-mode-alist
      (delete-dups
       (append
        '(("\\.go\\'" . go-ts-mode)
          ("\\.php\\'" . php-ts-mode)
          ("\\.rs\\'" . rust-ts-mode)
          ("\\.ya?ml\\'" . yaml-ts-mode)
          ("Dockerfile\\'" . dockerfile-ts-mode)
          ("CMakeLists.txt\\'" . cmake-ts-mode)
          ("\\.xaml\\'" . xml-mode)
          ("\\.ts\\'" . typescript-ts-mode)
          ("\\.mod\\'" . go-mod-ts-mode))
        auto-mode-alist)))

(setq inferior-lisp-program "sbcl")
(setq dired-dwim-target t)
(setq tramp-terminal-type "dumb")

(setq autosave-on-focus-lost-buffers (list))
(defun autosave-on-focus-lost-buffer ()
  (interactive)
  (let ((cur-buf (current-buffer)))
    (if (member cur-buf autosave-on-focus-lost-buffers)
        (delete cur-buf autosave-on-focus-lost-buffers)
      (add-to-list 'autosave-on-focus-lost-buffers cur-buf))))
(defun save-autosave-buffers ()
  (interactive)
  (if autosave-on-focus-lost-buffers
      (dolist (el autosave-on-focus-lost-buffers)
        (with-current-buffer el
          (save-buffer)))))
(add-hook 'focus-out-hook 'save-autosave-buffers)

(defun append-shell ()
  (let ((extension (file-name-extension buffer-file-name))
        (exists (file-exists-p buffer-file-name)))
    (cond ((equal extension "sh")
           (unless exists
             (insert "#!/usr/bin/sh\nset -u\n")))
          ((equal extension "py")
           (unless exists
             (insert "#!/usr/bin/env python3\n"))))))

(add-hook 'find-file-hook 'append-shell)
(add-hook 'gdb-mode-hook 'gdb-many-windows)

(defun kill-buffer--possibly-save (buffer)
  (y-or-n-p "buffer unsaved; kill it?"))

(defun eval-print-last-sexp (&optional eval-last-sexp-arg-internal)
  (interactive "P")
  (let ((standard-output (current-buffer)))
    (terpri)
    (insert ";; ")
    (eval-last-sexp t)
    (terpri)))

(defun icomplete-fido-backward-updir ()
  "Delete char before or go up directory, like `ido-mode'."
  (interactive)
  (call-interactively 'backward-delete-char))

(defun icomplete-fido-ret ()
  "Exit minibuffer or enter directory, like `ido-mode'."
  (interactive)
  (call-interactively 'icomplete-fido-exit))

(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator "/")
(setq delete-active-region nil)

(global-set-key (kbd "C-x f") 'project-root-find-file)

(setq grep-use-headings t)

(defun emerge-setup-windows (buffer-A buffer-B merge-buffer &optional pos)
  ;; Make sure we are not in the minibuffer window when we try to delete
  ;; all other windows.
  (if (eq (selected-window) (minibuffer-window))
      (other-window 1))
  (delete-other-windows)
  (switch-to-buffer merge-buffer)
  (emerge-refresh-mode-line)
  (split-window-below)
  (split-window-right)
  (switch-to-buffer buffer-A)
  (if pos
      (goto-char (point-min)))
  (other-window 1)
  (switch-to-buffer buffer-B)
  (if pos
      (goto-char (point-min)))
  (other-window 1)
  (if pos
      (goto-char (point-min)))
  ;; If diff/diff3 reports errors, display them rather than the merge
  ;; buffer.
  (if (/= 0 (with-current-buffer emerge-diff-error-buffer (buffer-size)))
      (message "Errors found in diff/diff3 output.  Merge buffer is %s."
                 (buffer-name emerge-merge-buffer))))
(remove-hook 'find-file-hooks 'vc-find-file-hook)

(setq split-height-threshold nil)
(setq split-width-threshold 0)
(setopt display-buffer-alist
        `((,(rx (| (: "*compilation*") (: "*grep*")))
           display-buffer-in-side-window
           (side . right)
           (window-width . 0.3)
           (window . main)
           (slot . 0)
           (window-parameters . ((no-delete-other-windows . t))))))
(setq display-buffer-base-action '
      ((display-buffer-reuse-window display-buffer-in-previous-window
                                    display-buffer-use-some-window)))
(setq image-dired-external-viewer "mpv")
(setq mark-ring-max 6)
(setq global-mark-ring-max 8)
(setopt set-mark-command-repeat-pop nil)
(setopt ls-lisp-dirs-first t)
(setopt ls-lisp-use-insert-directory-program nil)
;; One more reason to rewrite emacs.
(defun whitespace-write-file-hook () nil)
(setq-default isearch-lazy-count t)
(setq safe-local-variable-values
      '((display-line-numbers . visual) (display-line-numbers visual)))

(defun c4-git-acp ()
  (interactive)
  (start-process "git-acp" (get-buffer-create "*c4-git-acp*") "sh" "-c" "git add . && git commit -m \"commit\" && git push"))

(define-minor-mode c4-acp-on-save-mode
  "git add, commit and push on save"
  :init-value
  nil
  :lighter
  " git-acp"
  :after-hook
  (add-hook 'after-save-hook 'c4-git-acp))

(define-key global-map (kbd "M-i") 'insert-tab)
(setq browse-url-browser-function 'browse-url-xdg-open)
(setq markdown-indent-on-enter nil)
(setq org-directory "/notes")
(setq org-agenda-files `(,org-directory))
(setq org-default-notes-file (concat org-directory "/notes.org"))
(setq org-startup-folded t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-timestamp-if-don t)
(setq org-refile-use-outline-path 'file)
(setq org-refile-targets
      '((nil :maxlevel . 3)
        (org-agenda-files :maxlevel . 3)))
(setq org-bookmark-names-plist nil)
(setq org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))
(setq org-agenda-span 'week)
(setq org-habit-show-habits-only-for-today t)

(defun great-grand-parent-is (type)
  (lambda (_n parent &rest _)
    (let* ((grand-parent (treesit-node-parent parent))
           (great-grand-parent (treesit-node-parent grand-parent)))
      (if great-grand-parent
          (string-match-p
           type (treesit-node-type great-grand-parent))
        nil))))

(defun has-parent-node (type)
  (lambda (_n parent &rest _)
    ))

;; Did I already say that I hate emacs?
(defun my-c-ts-mode-indent-style ()
  )
(require 'c-ts-mode)
(defun c-ts-mode--standalone-grandparent (_n parent &rest _)
  (c-ts-mode--standalone-parent _n parent _))
(setopt
 treesit-simple-indent-override-rules
 `((c
    (c-ts-mode--for-each-tail-body-matcher prev-line c-ts-mode-indent-offset)

    ((parent-is "translation_unit") column-0 0)
    ((query "(ERROR (ERROR)) @indent") column-0 0)
    ((node-is ")") standalone-parent 0)
    ((node-is "]") parent-bol 0)
    ((node-is "else") parent-bol 0)
    ((node-is "case") parent-bol 0)
    ((node-is "preproc_arg") no-indent)
    ;; `c-ts-common-looking-at-star' has to come before
    ;; `c-ts-common-comment-2nd-line-matcher'.
    ((and (parent-is "comment") c-ts-common-looking-at-star)
     parent 1)
    (c-ts-common-comment-2nd-line-matcher standalone-parent c-ts-mode-indent-offset)
    ((parent-is "comment") parent-bol c-ts-mode-indent-offset)

    ;; Labels.
    ((node-is "labeled_statement") standalone-parent 0)
    ((parent-is "labeled_statement")
     c-ts-mode--standalone-grandparent c-ts-mode-indent-offset)

    ((great-grand-parent-is "parenthesized_expression") parent-bol ,(* 2 c-ts-mode-indent-offset))

    ;; Preproc directives
    ((node-is "preproc") column-0 0)
    ((node-is "#endif") column-0 0)
    ((match "preproc_call" "compound_statement") column-0 0)

    ;; Top-level things under a preproc directive.  Note that
    ;; "preproc" matches more than one type: it matches
    ;; preproc_if, preproc_elif, etc.
    ((n-p-gp nil "preproc" "translation_unit") column-0 0)
    ;; Indent rule for an empty line after a preproc directive.
    ((and no-node (parent-is ,(rx (or "\n" "preproc"))))
     c-ts-mode--standalone-parent c-ts-mode-indent-offset)
    ;; Statement under a preproc directive, the first statement
    ;; indents against parent, the rest statements indent to
    ;; their prev-sibling.
    ((match nil ,(rx "preproc_" (or "if" "elif")) nil 3 3)
     c-ts-mode--standalone-parent c-ts-mode-indent-offset)
    ((match nil "preproc_ifdef" nil 2 2)
     c-ts-mode--standalone-parent c-ts-mode-indent-offset)
    ((match nil "preproc_else" nil 1 1)
     c-ts-mode--standalone-parent c-ts-mode-indent-offset)
    ((parent-is "preproc") c-ts-mode--anchor-prev-sibling 0)

    ((parent-is "function_definition") parent-bol 0)
    ((parent-is "pointer_declarator") parent-bol 0)
    ((parent-is ,(rx bos "declaration" eos)) parent-bol 0)
    ((parent-is "conditional_expression") first-sibling 0)
    ((parent-is "assignment_expression") parent-bol c-ts-mode-indent-offset)
    ((parent-is "concatenated_string") first-sibling 0)
    ((parent-is "comma_expression") first-sibling 0)
    ((parent-is "init_declarator") parent-bol c-ts-mode-indent-offset)
    ((parent-is "parenthesized_expression") parent-bol c-ts-mode-indent-offset)
    ((parent-is "argument_list") standalone-parent c-ts-mode-indent-offset)
    ((parent-is "parameter_list") first-sibling 1)
    ((parent-is "binary_expression") c-ts-mode--standalone-grandparent c-ts-mode-indent-offset)
    ((query "(for_statement initializer: (_) @indent)") parent-bol c-ts-mode-indent-offset)
    ((query "(for_statement condition: (_) @indent)") parent-bol c-ts-mode-indent-offset)
    ((query "(for_statement update: (_) @indent)") parent-bol c-ts-mode-indent-offset)
    ((query "(call_expression arguments: (_) @indent)") parent c-ts-mode-indent-offset)
    ((parent-is "call_expression") parent 0)
    ;; Closing bracket.  This should be before initializer_list
    ;; (and probably others) rule because that rule (and other
    ;; similar rules) will match the closing bracket.  (Bug#61398)
    ((node-is "}") standalone-parent 0)

    ;; int[5] a = { 0, 0, 0, 0 };
    ((match nil "initializer_list" nil 1 1) parent-bol c-ts-mode-indent-offset)
    ((parent-is "initializer_list") c-ts-mode--anchor-prev-sibling 0)
    ;; Statement in enum.
    ((match nil "enumerator_list" nil 1 1) standalone-parent c-ts-mode-indent-offset)
    ((parent-is "enumerator_list") c-ts-mode--anchor-prev-sibling 0)
    ;; Statement in struct and union.
    ((match nil "field_declaration_list" nil 1 1) standalone-parent c-ts-mode-indent-offset)
    ((parent-is "field_declaration_list") c-ts-mode--anchor-prev-sibling 0)

    ;; Opening bracket.
    ((node-is "compound_statement") standalone-parent c-ts-mode-indent-offset)
    ;; Bug#61291.
    ((match "expression_statement" nil "body") standalone-parent c-ts-mode-indent-offset)
    ;; These rules are for cases where the body is bracketless.
    ;; Tested by the "Bracketless Simple Statement" test.
    ((parent-is "if_statement") standalone-parent c-ts-mode-indent-offset)
    ((parent-is "else_clause") standalone-parent c-ts-mode-indent-offset)
    ((parent-is "for_statement") standalone-parent c-ts-mode-indent-offset)
    ((match "while" "do_statement") parent-bol 0) ; (do_statement "while")
    ((parent-is "while_statement") standalone-parent c-ts-mode-indent-offset)
    ((parent-is "do_statement") standalone-parent c-ts-mode-indent-offset)

    ((parent-is "case_statement") standalone-parent c-ts-mode-indent-offset)

    ((node-is "ERROR") first-sibling 0)
    ((parent-is "ERROR") first-sibling 0)
    )))

(setq major-mode-remap-alist
 '((c-mode . c-ts-mode)
   (c++-mode . c++-ts-mode)
   ))
(setq markdown-css-paths '("markdown.css"))
(setopt latex-run-command "lualatex")
(setopt tex-run-command "luatex")
(setopt tex-directory "/tmp")
