#!/usr/bin/sh
export XKB_DEFAULT_OPTIONS="grp:lctrl_lwin_toggle,grp_led:caps,ctrl:nocaps,lv3:ralt_switch"
export XKB_DEFAULT_LAYOUT="us,ru"
export XKB_DEFAULT_VARIANT="dvp,ruu"
export XKB_DEFAULT_MODEL="pc105"
exec "${@}"
