vim.cmd('colorscheme lunaperche')
vim.cmd('highlight Normal ctermbg=NONE guibg=NONE')
vim.cmd('highlight EndOfBuffer ctermbg=NONE guibg=NONE')
vim.cmd('highlight MatchParen guifg=whitegray guibg=green gui=NONE ctermfg=white ctermbg=green cterm=NONE')
vim.cmd('highlight Whitespace guifg=gray ctermfg=gray')
vim.opt.compatible = false

vim.cmd('filetype indent off')
vim.opt.tabstop = 8
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 0
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.smartindent = true
vim.opt.autoindent = true
-- vim.opt.clipboard = "unnamedplus"
vim.g.editorconfig = true

vim.opt.incsearch = true
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.cursorline = true
vim.opt.cursorcolumn = false

vim.opt.list = true
vim.opt.listchars = {tab = '» ', space = '·', trail = '~', nbsp = '␣'}

vim.opt.modeline = true
vim.opt.bs = 'indent,eol,start'
vim.opt.showcmd = true
vim.opt.ruler = true
vim.opt.mouse = 'a'
vim.opt.title = true
vim.opt.autoread = true
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.infercase = true
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.encoding = 'utf-8'
vim.opt.wcm = vim.fn.char2nr(vim.api.nvim_replace_termcodes([[<TAB>]], true, true, true))
vim.opt.showmatch = false
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.colorcolumn = '75'
vim.opt.scrolloff = 999
vim.opt.foldmethod = 'marker'
vim.opt.inccommand = 'nosplit'
vim.opt.updatetime = 50
vim.opt.lazyredraw = false
vim.opt.ttyfast = true
vim.opt.termguicolors = true
vim.opt.linebreak = true
vim.opt.path:append("**")
vim.opt.makeprg = './build.sh'
vim.g.netrw_banner = 0
vim.opt.formatoptions["c"] = false
vim.opt.formatoptions["r"] = false
vim.opt.formatoptions["o"] = false

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = "*",
  callback = function(_)
    vim.opt.formatoptions:remove({"c", "r", "o"})
  end
})

if vim.g.neovide then
  vim.opt.guifont = "Liberation Mono:h10"
  vim.opt.linespace = 0
  vim.g.neovide_transparency = 0.9
  vim.g.neovide_refresh_rate = 120
  vim.g.neovide_cursor_animation_length = 0.02
  vim.g.neovide_cursor_trail_size = 0.4
end

vim.api.nvim_create_autocmd("FileType", {
  pattern = "go",
  callback = function()
    vim.bo.makeprg = "go build main.go 2>&1"
    vim.bo.errorformat = "%f:%l:%c: %m"
  end,
})

vim.api.nvim_create_autocmd("FileType", {
  pattern = {"cs", "sln", "csproj"},
  callback = function()
    vim.bo.makeprg = "./build.sh 2>&1"
    vim.bo.errorformat = "%*[ \t]%f(%l\\,%c): %t%*[^:] %m"
  end,
})

vim.api.nvim_create_autocmd("FileType", {
  pattern = { "lua", "javascript", "json" },
  callback = function()
    vim.bo.shiftwidth = 2
  end,
})

vim.api.nvim_create_user_command(
  "Ctoggle",
  function()
    for _, p in ipairs(vim.fn.getwininfo()) do
      if p["quickfix"] ~= 0 then
        vim.cmd("cclose")
        return
      end
    end
    vim.cmd("copen")
  end,
  {}
)

vim.api.nvim_create_user_command(
  "Cm",
  function()
    vim.opt.shiftwidth = 8
    vim.opt.expandtab = false
  end,
  {}
)

vim.g.mapleader = ' '
vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)
vim.keymap.set('n', '<M-n>', '<cmd>cnext<cr>')
vim.keymap.set('n', '<M-p>', '<cmd>cprevious<cr>')
vim.keymap.set('n', '<leader>ct', '<cmd>Ctoggle<cr>')
vim.keymap.set('n', '<leader>nh', '<cmd>noh<cr>')
vim.keymap.set('n', '<leader>bn', '<cmd>bnext<cr>')
vim.keymap.set('n', '<leader>bp', '<cmd>bprevious<cr>')
vim.keymap.set('i', '<C-Space>', '<C-X><C-O>')
vim.keymap.set('n', 'K', vim.lsp.buf.hover)
vim.keymap.set('n', 'cr', vim.lsp.buf.rename)
vim.keymap.set('n', 'ca', vim.lsp.buf.code_action)
vim.keymap.set('n', 'cu', vim.lsp.buf.references)
vim.keymap.set('n', 'ch', vim.lsp.buf.signature_help)

require("c4/decode-gpg")
require("c4/async_make")
require("c4/plug")
require("c4/plug/telescope")
require("c4/plug/tree-sitter")
