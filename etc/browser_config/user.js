

// Personal
user_pref("browser.uidensity", 1);
user_pref("ui.prefersReducedMotion", 1);
user_pref("ui.systemUsesDarkTheme", 1);
user_pref("accessibility.typeaheadfind", false);
user_pref("clipboard.autocopy", true);
user_pref("layout.spellcheckDefault", 2);
user_pref("browser.backspace_action", 2);
user_pref("browser.quitShortcut.disabled", false);
user_pref("browser.tabs.closeWindowWithLastTab", false);
user_pref("browser.tabs.loadBookmarksInTabs", true);
user_pref("browser.urlbar.decodeURLsOnCopy", true);
user_pref("general.autoScroll", true);
user_pref("identity.fxaccounts.enabled", false);
user_pref("browser.bookmarks.max_backups", 2);
user_pref("media.default_volume", "1");
user_pref("privacy.webrtc.hideGlobalIndicator", true);
user_pref("browser.eme.ui.enabled", false); // Disable drm notice
user_pref("browser.altClickSave", true);
user_pref("xpinstall.signatures.required", false);
user_pref("privacy.resistFingerprinting.block_mozAddonManager", true);
user_pref("extensions.webextensions.restrictedDomains", "");
user_pref("security.sandbox.content.read_path_whitelist", "/dev/snd/");
user_pref("security.sandbox.content.write_path_whitelist", "/dev/snd/");
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("security.fileuri.strict_origin_policy", false); // disable CORS
user_pref("widget.gtk.hide-pointer-while-typing.enabled", false);
user_pref("ui.caretBlinkTime", 0);
user_pref("browser.cache.memory.enable", true);
user_pref("dom.ipc.processCount", 4);
user_pref("browser.preferences.defaultPerformanceSettings.enabled", true);
user_pref("full-screen-api.warning.timeout", 0);
user_pref("findbar.highlightAll", true);
user_pref("font.internaluseonly.changed", true);
user_pref("font.minimum-size.x-western", 10);
user_pref("font.name.monospace.x-cyrillic", "Liberation Mono");
user_pref("font.name.monospace.x-western", "Liberation Mono");
user_pref("font.name.sans-serif.x-cyrillic", "Liberation Mono");
user_pref("font.name.sans-serif.x-western", "Liberation Mono");
user_pref("font.name.serif.x-cyrillic", "Liberation Mono");
user_pref("font.name.serif.x-western", "Liberation Mono");
user_pref("font.size.monospace.x-cyrillic", 14);
user_pref("font.size.monospace.x-western", 14);
user_pref("browser.display.use_document_fonts", 0);
user_pref("browser.toolbars.bookmarks.visibility", "never");
user_pref("browser.proton.toolbar.version", 3);
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);
user_pref("browser.download.always_ask_before_handling_new_types", true);
user_pref("browser.download.useDownloadDir", false);
user_pref("browser.download.dir", "/home/workflow");
user_pref("findbar.highlightAll", true);

user_pref("browser.uiCustomization.state", '{"placements":{"widget-overflow-fixed-list":[],"unified-extensions-area":["ff2mpv_yossarian_net-browser-action","myallychou_gmail_com-browser-action","_7be2ba16-0f1e-4d93-9ebc-5164397477a9_-browser-action","_3c078156-979c-498b-8990-85f7987dd929_-browser-action","_762f9885-5a13-4abd-9c77-433dcd38b8fd_-browser-action","_9a253c57-0e95-4589-be64-365b3602c564_-browser-action","_9a41dee2-b924-4161-a971-7fb35c053a4a_-browser-action","_74145f27-f039-47ce-a470-a662b129930a_-browser-action"],"nav-bar":["back-button","stop-reload-button","forward-button","zoom-controls","urlbar-container","search-container","downloads-button","fxa-toolbar-menu-button","unified-extensions-button","reset-pbm-toolbar-button","firefox_tampermonkey_net-browser-action","_aecec67f-0d10-4fa7-b7c7-609a2db280cf_-browser-action","foxyproxy_eric_h_jung-browser-action","ublock0_raymondhill_net-browser-action","umatrix_raymondhill_net-browser-action","numatrix_arek_codes-browser-action","_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action"],"toolbar-menubar":["menubar-items"],"TabsToolbar":["tabbrowser-tabs","new-tab-button","alltabs-button"],"vertical-tabs":[],"PersonalToolbar":["import-button","personal-bookmarks"]},"seen":["developer-button","ff2mpv_yossarian_net-browser-action","myallychou_gmail_com-browser-action","_7be2ba16-0f1e-4d93-9ebc-5164397477a9_-browser-action","firefox_tampermonkey_net-browser-action","foxyproxy_eric_h_jung-browser-action","ublock0_raymondhill_net-browser-action","umatrix_raymondhill_net-browser-action","_3c078156-979c-498b-8990-85f7987dd929_-browser-action","_762f9885-5a13-4abd-9c77-433dcd38b8fd_-browser-action","_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action","_9a253c57-0e95-4589-be64-365b3602c564_-browser-action","_9a41dee2-b924-4161-a971-7fb35c053a4a_-browser-action","_74145f27-f039-47ce-a470-a662b129930a_-browser-action","numatrix_arek_codes-browser-action","_aecec67f-0d10-4fa7-b7c7-609a2db280cf_-browser-action"],"dirtyAreaCache":["PersonalToolbar","nav-bar","toolbar-menubar","TabsToolbar","unified-extensions-area","vertical-tabs"],"currentVersion":20,"newElementCount":3}');

// colors

user_pref("browser.anchor_color", "#1c71d8");
user_pref("browser.display.background_color", "#f6f5f4");
user_pref("browser.display.document_color_use", 1);
user_pref("browser.display.foreground_color", "#000000");
user_pref("browser.visited_color", "#26a269");


// user_pref("browser.anchor_color", "#1eb0f0");
// user_pref("browser.display.background_color", "#353535");
// user_pref("browser.display.document_color_use", 0);
// user_pref("browser.display.foreground_color", "#f0f0f0");
// user_pref("browser.visited_color", "#26d432");

// xdg
user_pref("widget.use-xdg-desktop-portal.file-picker", 1);
user_pref("widget.use-xdg-desktop-portal.mime-handler", 1);
user_pref("widget.use-xdg-desktop-portal.open-uri", 1);

// HSTS
user_pref("security.mixed_content.block_display_content", true);
user_pref("network.stricttransportsecurity.preloadlist", false);

// BetterFox
user_pref("nglayout.initialpaint.delay", 0); // default=5; used to be 250
user_pref("nglayout.initialpaint.delay_in_oopif", 0); // default=5
user_pref("content.notify.interval", 100000); // (.10s); default=120000 (.12s)
user_pref("layout.css.grid-template-masonry-value.enabled", true);
user_pref("dom.enable_web_task_scheduling", true);
user_pref("layout.css.has-selector.enabled", true);
user_pref("gfx.canvas.accelerated.cache-items", 4096); // default=2048; alt=8192
user_pref("gfx.canvas.accelerated.cache-size", 512); // default=256; alt=1024
user_pref("gfx.content.skia-font-cache-size", 20); // default=5; Chrome=20
user_pref("media.memory_cache_max_size", 65536); // default=8192; AF=65536; alt=131072
user_pref("media.cache_readahead_limit", 7200); // 120 min; default=60; stop reading ahead when our buffered data is this many seconds ahead of the current playback
user_pref("media.cache_resume_threshold", 3600); // 60 min; default=30; when a network connection is suspended, don't resume it until the amount of buffered data falls below this threshold
user_pref("image.mem.decode_bytes_at_a_time", 32768); // default=16384; alt=65536; chunk size for calls to the image decoders
user_pref("network.buffer.cache.size", 262144); // 256 kb; default=32768 (32 kb)
user_pref("network.buffer.cache.count", 128); // default=24
user_pref("network.http.max-connections", 1800); // default=900
user_pref("network.http.max-persistent-connections-per-server", 10); // default=6; download connections; anything above 10 is excessive
user_pref("network.http.max-urgent-start-excessive-connections-per-host", 5); // default=3
user_pref("network.websocket.max-connections", 400); // default=200
user_pref("network.http.pacing.requests.enabled", false);
user_pref("network.dnsCacheEntries", 10000); // default=400
user_pref("network.dnsCacheExpiration", 86400); // keep entries for 1 day; alt=3600 (1 hour)
user_pref("network.dns.max_high_priority_threads", 8); // default=5
user_pref("network.ssl_tokens_cache_capacity", 20480); // default=2048; more TLS token caching (fast reconnects)
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.prefetch-next", false);
user_pref("network.predictor.enabled", false);
user_pref("network.predictor.enable-prefetch", false);

// new
user_pref("browser.urlbar.update2.engineAliasRefresh", true);

// Bookmarks
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.bookmarks.showMobileBookmarks", false);

// Ctrl+tab
user_pref("browser.ctrlTab.previews", false);

// Download UI
user_pref("browser.download.autohideButton", false);
user_pref("browser.download.panel.shown", true);

// Newpage content
user_pref("browser.library.activity-stream.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.places", true);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.filterAdult", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry.ping.endpoint", "");
user_pref("browser.newtabpage.activity-stream.tippyTop.service.endpoint", "");
user_pref("browser.newtabpage.activity-stream.topSitesRows", 3);
user_pref("browser.newtabpage.enhanced", true);

// Telemery
user_pref("browser.ping-centre.telemetry", false);
user_pref("security.ssl.errorReporting.automatic", true);
user_pref("toolkit.identity.enabled", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.coverage.opt-out", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("toolkit.telemetry.infoURL", "");
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);

// Disable safe browsing
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous_host", false);
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.provider.google.advisoryURL", "");
user_pref("browser.safebrowsing.provider.google.gethashURL", "");
user_pref("browser.safebrowsing.provider.google.lists", "");
user_pref("browser.safebrowsing.provider.google.pver", "");
user_pref("browser.safebrowsing.provider.google.reportMalwareMistakeURL", "");
user_pref("browser.safebrowsing.provider.google.reportPhishMistakeURL", "");
user_pref("browser.safebrowsing.provider.google.reportURL", "");
user_pref("browser.safebrowsing.provider.google.updateURL", "");
user_pref("browser.safebrowsing.provider.google4.advisoryName", "");
user_pref("browser.safebrowsing.provider.google4.advisoryURL", "");
user_pref("browser.safebrowsing.provider.google4.dataSharingURL", "");
user_pref("browser.safebrowsing.provider.google4.gethashURL", "");
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "");
user_pref("browser.safebrowsing.provider.google4.lists", "");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "");
user_pref("browser.safebrowsing.provider.google4.pver", "");
user_pref("browser.safebrowsing.provider.google4.reportMalwareMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.reportPhishMistakeURL", "");
user_pref("browser.safebrowsing.provider.google4.reportURL", "");
user_pref("browser.safebrowsing.provider.google4.updateURL", "");
user_pref("browser.safebrowsing.provider.mozilla.gethashURL", "");
user_pref("browser.safebrowsing.provider.mozilla.lists", "");
user_pref("browser.safebrowsing.provider.mozilla.pver", "");
user_pref("browser.safebrowsing.provider.mozilla.updateURL", "");

// Search
user_pref("browser.search.countryCode", "US");
user_pref("browser.search.geoSpecificDefaults", false);
user_pref("browser.search.geoSpecificDefaults.url", "");
user_pref("browser.search.geoip.url", "");
user_pref("browser.search.hiddenOneOffs", "Bing,Amazon.com,Twitter");
user_pref("browser.search.region", "US");
user_pref("geo.wifi.uri", "");

// Tabs
user_pref("browser.tabs.loadInBackground", true);
user_pref("browser.tabs.tabMinWidth", 30);
user_pref("browser.tabs.warnOnClose", false);

// URL bar
user_pref("browser.urlbar.clickSelectsAll", true);
user_pref("browser.urlbar.maxRichResults", 15);
user_pref("browser.urlbar.trimURLs", false);

// Mozilla telemetry
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("datareporting.policy.firstRunURL", "");

// Sensors
user_pref("device.sensors.enabled", false);
user_pref("device.sensors.motion.enabled", false);
user_pref("device.sensors.orientation.enabled", false);

// DevTools
user_pref("devtools.aboutdebugging.showSystemAddons", true);
user_pref("devtools.onboarding.telemetry.logged", false);
user_pref("devtools.theme", "dark");
user_pref("devtools.toolbox.splitconsoleEnabled", false);

// Push-notifications
user_pref("dom.push.enabled", false);
user_pref("permissions.default.desktop-notification", 2);
user_pref("permissions.default.geo", 2);

// Experemental
user_pref("experiments.activeExperiment", true);
user_pref("experiments.enabled", true);
user_pref("experiments.supported", true);

// Extensions
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.ui.dictionary.hidden", false);
user_pref("extensions.ui.experiment.hidden", false);
user_pref("extensions.ui.locale.hidden", false);
user_pref("extensions.webextensions.remote", true);

// Fonts
user_pref("findbar.highlightAll", true);
user_pref("font.internaluseonly.changed", true);
user_pref("font.minimum-size.x-western", 10);
user_pref("font.name.monospace.x-western", "Liberation Mono");
user_pref("font.name.sans-serif.x-western", "Liberation Mono");
user_pref("font.name.serif.x-western", "Liberation Mono");

// (WARN) User-agent
user_pref("general.useragent.override", "Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0");
user_pref("general.useragent.vendor", "");
user_pref("general.useragent.vendorSub", "");

// about:config
user_pref("general.warnOnAboutConfig", false);

// Render
user_pref("gfx.use_text_smoothing_setting", true);
user_pref("gfx.webrender.enabled", true);
user_pref("gfx.webrender.highlight-painted-layers", false);
user_pref("layers.acceleration.force-enabled", true);

// Languages
user_pref("intl.accept_languages", "en-us,en,ru");
user_pref("intl.locale.requested", "en-US");

// Video
user_pref("media.autoplay.enabled", false);
user_pref("media.av1.enabled", true);

// Network
user_pref("network.allow-experiments", true);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.dns.echconfig.enabled", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.predictor.enabled", false);
user_pref("network.prefetch-next", false);
user_pref("network.tcp.tcp_fastopen_enable", true);
user_pref("network.trr.uri", "https://mozilla.cloudflare-dns.com/dns-query");
user_pref("network.warnOnAboutNetworking", false);

// Private
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.firstparty.isolate", true);
user_pref("privacy.resistFingerprinting", false); // I love dark themes more than my privacy
user_pref("privacy.resistFingerprinting.autoDeclineNoUserInputCanvasPrompts", false);

// Text reader
user_pref("reader.color_scheme", "dark");
user_pref("reader.content_width", 12);

// Dark theme
user_pref("widget.chrome.allow-gtk-dark-theme", true);
user_pref("widget.content.allow-gtk-dark-theme", true);

/// User.js by arkenfox

// Disable check default browser
user_pref("browser.shell.checkDefaultBrowser", false);

// Starup page
user_pref("browser.startup.page", 3);
user_pref("browser.startup.homepage", "about:blank");
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtab.preload", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false); // [DEFAULT: false]
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.feeds.discoverystreamfeed", false); // [FF66+]
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false); // [FF83+]
user_pref("browser.newtabpage.activity-stream.default.sites", "");

// Geolocation
user_pref("geo.provider.network.url", "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%");
user_pref("geo.provider.use_gpsd", false);
user_pref("browser.region.update.enabled", false);
user_pref("browser.region.network.url", "");
user_pref("javascript.use_us_english_locale", true);

// Quiter fox
user_pref("browser.search.update", false);
user_pref("extensions.getAddons.showPane", false);
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false);
user_pref("browser.discovery.enabled", false);

// Telemetry
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);

user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false); // see [NOTE]
user_pref("toolkit.telemetry.server", "data:,");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false); // [FF55+]
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false); // [FF55+]
user_pref("toolkit.telemetry.updatePing.enabled", false); // [FF56+]
user_pref("toolkit.telemetry.bhrPing.enabled", false); // [FF57+] Background Hang Reporter
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false); // [FF57+]

user_pref("toolkit.telemetry.coverage.opt-out", true); // [HIDDEN PREF]
user_pref("toolkit.coverage.opt-out", true); // [FF64+] [HIDDEN PREF]
user_pref("toolkit.coverage.endpoint.base", "");

user_pref("browser.ping-centre.telemetry", false);

// Studies
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.api_url", "");

// Crash reports
user_pref("breakpad.reportURL", "");
user_pref("browser.tabs.crashReporting.sendReport", false); // [FF44+]
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false); // [DEFAULT: false]

// Other
user_pref("captivedetect.canonicalURL", "");
user_pref("network.captive-portal-service.enabled", false); // [FF52+]

user_pref("network.connectivity-service.enabled", false);

user_pref("extensions.webcompat-reporter.enabled", false); // [DEFAULT: false]

// DNS
user_pref("network.proxy.socks_remote_dns", true);
user_pref("network.file.disable_unc_paths", true); // [HIDDEN PREF]
user_pref("network.gio.supported-protocols", ""); // [HIDDEN PREF]

// Search bar
user_pref("keyword.enabled", false);
user_pref("browser.fixup.alternate.enabled", false);
user_pref("browser.fixup.alternate.prefix", "");
user_pref("browser.fixup.alternate.protocol", "");
user_pref("browser.fixup.alternate.suffix", "");
user_pref("browser.fixup.fallback-to-https", false);
user_pref("browser.urlbar.trimURLs", false);
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.suggest.searches", false);
user_pref("browser.urlbar.speculativeConnect.enabled", false);
user_pref("browser.urlbar.dnsResolveSingleWordsAfterSearch", 0);
user_pref("browser.urlbar.suggest.quicksuggest.nonsponsored", false); // [FF95+]
user_pref("browser.urlbar.suggest.quicksuggest.sponsored", false);
user_pref("browser.urlbar.suggest.engines", false);

user_pref("browser.formfill.enable", false);
user_pref("extensions.formautofill.addresses.enabled", false); // [FF55+]
user_pref("extensions.formautofill.available", "off"); // [FF56+]
user_pref("extensions.formautofill.creditCards.available", false); // [FF57+]
user_pref("extensions.formautofill.creditCards.enabled", false); // [FF56+]
user_pref("extensions.formautofill.heuristics.enabled", false); // [FF55+]

user_pref("layout.css.visited_links_enabled", false);

user_pref("browser.compactmode.show", true);

// Paswords
user_pref("signon.autofillForms", false);
user_pref("signon.generation.enabled", false); // disable password generation
user_pref("signon.firefoxRelay.feature", false); // some firefox relay, idk
user_pref("signon.management.page.breach-alerts.enabled", false);

// Disk
user_pref("browser.cache.disk.enable", false);
user_pref("browser.privatebrowsing.forceMediaMemoryCache", true); // [FF75+]
user_pref("media.memory_cache_max_size", 65536);
user_pref("browser.sessionstore.interval", 30000); // [DEFAULT: 15000]
user_pref("browser.sessionstore.privacy_level", 2);
user_pref("browser.shell.shortcutFavicons", false);

// Secure
user_pref("security.ssl.require_safe_negotiation", true);
user_pref("security.tls.enable_0rtt_data", false);
user_pref("security.OCSP.enabled", 0); // [DEFAULT: 1]
user_pref("security.OCSP.require", false);
user_pref("security.pki.sha1_enforcement_level", 1);
user_pref("security.family_safety.mode", 0);
user_pref("security.cert_pinning.enforcement_level", 2);
user_pref("security.remote_settings.crlite_filters.enabled", true);
user_pref("security.pki.crlite_mode", 2);

// UI
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
user_pref("browser.ssl_override_behavior", 1);
user_pref("browser.xul.error_pages.expert_bad_cert", true);
user_pref("security.insecure_connection_text.enabled", true); // [FF60+]

// Fonts
user_pref("gfx.font_rendering.opentype_svg.enabled", false);

// DOM
user_pref("dom.disable_beforeunload", true);
user_pref("dom.disable_window_move_resize", true);

// Misc
user_pref("accessibility.force_disabled", 1);
user_pref("beacon.enabled", false);
user_pref("browser.helperApps.deleteTempFileOnExit", true);
user_pref("browser.pagethumbnails.capturing_disabled", true); // [HIDDEN PREF]
user_pref("browser.uitour.enabled", false);
user_pref("browser.uitour.url", "");
user_pref("permissions.manager.defaultsUrl", "");
user_pref("webchannel.allowObject.urlWhitelist", "");
user_pref("network.IDN_show_punycode", true);
user_pref("pdfjs.disabled", false); // [DEFAULT: false]
user_pref("browser.download.alwaysOpenPanel", false);
user_pref("browser.download.manager.addToRecentDocs", false);

// Fingerprinting
user_pref("privacy.window.maxInnerWidth", 1600);
user_pref("privacy.window.maxInnerHeight", 900);
user_pref("privacy.resistFingerprinting.letterboxing", true); // [HIDDEN PREF]
user_pref("layout.css.font-visibility.resistFingerprinting", 1); // [DEFAULT: 1]
user_pref("browser.startup.blankWindow", false);
user_pref("browser.display.use_system_colors", false); // [DEFAULT false NON-WINDOWS]
user_pref("widget.non-native-theme.enabled", true); // [DEFAULT: true]
user_pref("browser.link.open_newwindow.restriction", 0);
user_pref("browser.link.open_newwindow", 3); // [DEFAULT: 3]
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);



// user_pref("general.smoothScroll.msdPhysics.continuousMotionMaxDeltaMS",   12);//NSS    [120]
// user_pref("general.smoothScroll.msdPhysics.enabled",                    true);//NSS  [false]
// user_pref("general.smoothScroll.msdPhysics.motionBeginSpringConstant",   200);//NSS   [1250]
// user_pref("general.smoothScroll.msdPhysics.regularSpringConstant",       250);//NSS   [1000]
// user_pref("general.smoothScroll.msdPhysics.slowdownMinDeltaMS",           25);//NSS     [12]
// user_pref("general.smoothScroll.msdPhysics.slowdownMinDeltaRatio",     "2.0");//NSS    [1.3]
// user_pref("general.smoothScroll.msdPhysics.slowdownSpringConstant",      250);//NSS   [2000]
// user_pref("general.smoothScroll.currentVelocityWeighting",             "1.0");//NSS ["0.25"]
// user_pref("general.smoothScroll.stopDecelerationWeighting",            "1.0");//NSS  ["0.4"]

// /// adjust multiply factor for mousewheel - or set to false if scrolling is way too fast
// user_pref("mousewheel.system_scroll_override.horizontal.factor",         200);//NSS    [200]
// user_pref("mousewheel.system_scroll_override.vertical.factor",           200);//NSS    [200]
// user_pref("mousewheel.system_scroll_override_on_root_content.enabled",  true);//NSS   [true]
// user_pref("mousewheel.system_scroll_override.enabled",                  true);//NSS   [true]

// /// adjust pixels at a time count for mousewheel - cant do more than a page at once if <100
// user_pref("mousewheel.default.delta_multiplier_x",                       100);//NSS    [100]
user_pref("mousewheel.default.delta_multiplier_y",                       100);//NSS    [100]
// user_pref("mousewheel.default.delta_multiplier_z",                       100);//NSS    [100]

// ///  this preset will reset couple extra variables for consistency
// user_pref("apz.allow_zooming",                                          true);//NSS   [true]
// user_pref("apz.force_disable_desktop_zooming_scrollbars",              false);//NSS  [false]
// user_pref("apz.paint_skipping.enabled",                                 true);//NSS   [true]
// user_pref("apz.windows.use_direct_manipulation",                        true);//NSS   [true]
// user_pref("dom.event.wheel-deltaMode-lines.always-disabled",           false);//NSS  [false]
// user_pref("general.smoothScroll.durationToIntervalRatio",                200);//NSS    [200]
// user_pref("general.smoothScroll.lines.durationMaxMS",                    150);//NSS    [150]
// user_pref("general.smoothScroll.lines.durationMinMS",                    150);//NSS    [150]
// user_pref("general.smoothScroll.other.durationMaxMS",                    150);//NSS    [150]
// user_pref("general.smoothScroll.other.durationMinMS",                    150);//NSS    [150]
// user_pref("general.smoothScroll.pages.durationMaxMS",                    150);//NSS    [150]
// user_pref("general.smoothScroll.pages.durationMinMS",                    150);//NSS    [150]
// user_pref("general.smoothScroll.pixels.durationMaxMS",                   150);//NSS    [150]
// user_pref("general.smoothScroll.pixels.durationMinMS",                   150);//NSS    [150]
// user_pref("general.smoothScroll.scrollbars.durationMaxMS",               150);//NSS    [150]
// user_pref("general.smoothScroll.scrollbars.durationMinMS",               150);//NSS    [150]
// user_pref("general.smoothScroll.mouseWheel.durationMaxMS",               200);//NSS    [200]
// user_pref("general.smoothScroll.mouseWheel.durationMinMS",                50);//NSS     [50]
// user_pref("layers.async-pan-zoom.enabled",                              true);//NSS   [true]
// user_pref("layout.css.scroll-behavior.spring-constant",                "250");//NSS    [250]
// user_pref("mousewheel.transaction.timeout",                             1500);//NSS   [1500]
// user_pref("mousewheel.acceleration.factor",                               10);//NSS     [10]
// user_pref("mousewheel.acceleration.start",                                -1);//NSS     [-1]
// user_pref("mousewheel.min_line_scroll_amount",                             5);//NSS      [5]
// user_pref("toolkit.scrollbox.horizontalScrollDistance",                    5);//NSS      [5]
// user_pref("toolkit.scrollbox.verticalScrollDistance",                      3);//NSS      [3]


user_pref("devtools.command-button-noautohide.enabled", true);
// user_pref("dom.event.clipboardevents.enabled", false);

user_pref("network.http.max-connections", 1800); // default=900
user_pref("network.http.max-connections-per-server", 32);
user_pref("network.http.max-persistent-connections-per-server", 12); // default=6
user_pref("network.http.max-urgent-start-excessive-connections-per-host", 10); // default=3
user_pref("network.http.pacing.requests.burst", 32); // default=10
user_pref("network.http.pacing.requests.min-parallelism", 10); // default=6
user_pref("network.websocket.max-connections", 400); // default=200

user_pref("webgl.disabled", false);
user_pref("privacy.clearOnShutdown_v2.cookiesAndStorage", false);
user_pref("dom.security.https_only_mode", false);
user_pref("dom.security.https_only_mode_ever_enabled", true);

user_pref("dom.security.https_first", true);
user_pref("dom.security.https_first_add_exception_on_failiure", false);
user_pref("dom.security.https_first_pbm", true);
user_pref("dom.security.https_first_schemeless", false);
user_pref("widget.wayland-dmabuf-vaapi.enabled", true);
user_pref("media.ffvpx.enabled", false);
user_pref("browser.download.open_pdf_attachments_inline", true);
user_pref("browser.download.forbid_open_with", true);
user_pref("browser.download.always_ask_before_handling_new_types", true);
user_pref("network.dns.native-is-localhost", false);
user_pref("network.dns.disableIPv6", false);
user_pref("network.trr.mode", 5);

// Apparently you just can't get video acceleration in 2025 without selling your soul to devil or something.
// user_pref("media.rdd-ffmpeg.enabled", false); // Disable video acceleration to fix youtube freezes
user_pref("media.rdd-ffmpeg.enabled", true);
// False alarm, it's network problems

user_pref("network.buffer.cache.size", 67072); // 1 mb; default=32768 (32 kb)
user_pref("network.buffer.cache.count", 96); // default=24
user_pref("network.http.max-connections", 3200); // default=900
user_pref("network.http.max-persistent-connections-per-server", 10); // default=6; download connections; anything above 10 is excessive
user_pref("network.http.max-urgent-start-excessive-connections-per-host", 10); // default=3
user_pref("network.websocket.max-connections", 800); // default=200
